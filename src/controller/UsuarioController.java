package controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import entidades.Artista;
import entidades.Usuario;
import entidades.Video;
import modelo.dao.ArtistaRepo;
import modelo.negocio.GestionArtista;
import modelo.negocio.GestionUsuario;
import modelo.negocio.GestionVideo;

@Controller
@SessionAttributes(names={"usuario"})
public class UsuarioController {
	@Autowired
	GestionUsuario gestion;
	
	//Pruebas
	@Autowired
	GestionArtista ga;
	@Autowired
	GestionVideo gv;

	/************* Controller de registro ***************************/
	@RequestMapping("/registrarUsuario.do")
	public String rellenarFormUsuario(Model modelo) {
		modelo.addAttribute("usuario", new Usuario());
		return "registro";
	}

	@PostMapping("/registrarUsuario.do")
	public String registrar(@ModelAttribute("usuario") Usuario usuario, BindingResult errores) {
		if (!errores.hasErrors()) {

			if (!gestion.registrarUsuario(usuario)) {
				errores.rejectValue("email", "repetido");
				return "registro";
			} else {
				return "redirect:/login.do";
			}
		}
		return "registro";
	}
	// Fin controller registro

	/******************* Controller login**************************** */
	@RequestMapping("/login.do")
	public String rellenarForm(Model modelo) {
		modelo.addAttribute("usuario", new Usuario());
		return "login";
	}

	/******************* Controller index**************************** */

	@RequestMapping(value = "/index.do", method = RequestMethod.GET)
	public String indexUsuario(@ModelAttribute("usuario") Usuario user, HttpServletRequest request) {
		return "index";

	}
	@RequestMapping(value = "/index.do", method = RequestMethod.POST)
	public String logarUsuario(@ModelAttribute("usuario") Usuario user, BindingResult binding,
			HttpServletRequest request) {
		if (!binding.hasErrors()) {
			if (gestion.logarUsuario(user) != null) {
				HttpSession sesion = request.getSession(true);
				sesion.setMaxInactiveInterval(1200);
				user = gestion.recuperarUsuario(user.getEmail());
				sesion.setAttribute("usuario", user);
				//List<Artista> artistas= ga.recuperar12mejoresArtistas();
				List<Artista> artistas= ga.recuperarArtistas();
				sesion.setAttribute("artistas", artistas);
				List<Video> videos = gv.recuperarVideos();
				sesion.setAttribute("videos", videos);
				return "index";
			} else {
				binding.rejectValue("email", "errorLogin");
				return "login";
			}
		}
		return "registro";
	}
	// Fin controller login

	@RequestMapping("/cerrarSesion.do")
	public String cerrarSesion(HttpServletRequest request) {
		HttpSession sesion = request.getSession(false);
		if (sesion != null) {
			sesion.removeAttribute("usuario");
			sesion.invalidate();
		}
		return "login";
	}

	@RequestMapping(value = "/agregarArtista.do", method = RequestMethod.POST)
	public String agrearArtista(@ModelAttribute("usuario") Usuario usuario, Artista artista) {
		gestion.aniadirArtista(usuario, artista.getIdArtista());
		return "";
	}

	@RequestMapping(value = "/quitarArtista.do", method = RequestMethod.POST)
	public String eliminarArtista(@ModelAttribute("usuario") Usuario usuario, Artista artista) {
		gestion.eliminarArtista(usuario, artista.getIdArtista());
		return "";
	}



	public GestionUsuario getGestion() {
		return gestion;
	}

	public void setGestion(GestionUsuario gestion) {
		this.gestion = gestion;
	}

}
