package controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import aplicacion.json.lastfm.Imagen;
import aplicacion.json.lastfm.Mensaje2;
import aplicacion.json.youtube.Items2;
import aplicacion.json.youtube.Mensaje3;
import entidades.Artista;
import entidades.Video;
import modelo.dao.ArtistaRepo;
import modelo.dao.VideoRepo;

@Controller
public class RobarController2 {
	// Trabajando con SessionAttributes

	@Autowired
	ArtistaRepo artistaRepo;
	@Autowired
	VideoRepo videoRepo;

	@RequestMapping("/robarVideos2.do")
	public String login() {
		// Para estad�stica de videos y artistas recuperados
		int numVideos = 0;
		int numArtistas = 0;

		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);

		/*********************************************************************
		 * La API de Youtube neceista un formato de fechas espec�fico que es el
		 * siguiente: yyyy-MM-ddTHH:mm:ssz de ah� que tenga que hacer los
		 * replace, para quitar el espacio por T y a�adir al final la z
		 *********************************************************************/
		WebTarget target = client.target(getBaseURI());
		Mensaje3 mensaje = target.request().accept(MediaType.APPLICATION_JSON).get(Mensaje3.class);
		Items2 item1 = new Items2();
		// try {
			do {
				for (int i = 0; i < mensaje.getItems().size() - 1; i++) {
					item1 = mensaje.getItems().get(i);
					String[] cadena;
					String artista;
					String titulo;
					String id;
					String artistaVideo;
					String nombreCanal;
					String fechaPublicacion;
					
					// Como queremos Artist - Nombre video separamos la cadena
					// que nos envia la API
					cadena = item1.getSnippet().getTitle().split("-");
					// Si no tiene - no cumple con nuestro prop�sito y lo
					// saltamos
					if (cadena.length > 1) {
						System.out.println("***************** VIDEO ********************");
						// Comprobamos si ya exite el video en la bbdd
						if (videoRepo.findByUrlVideo(item1.getSnippet().getResourceId().getVideoId()) == null) {
							// La primera cadena es el artista, el resto t�tulo ( artista - titulo )
							artista = cadena[0];
							artista = artista.replace("\\", "").replace("?", "").replace("/", "").trim();
							String[] artistas = artista.split(",");
							titulo = cadena[1].trim();
							id = item1.getSnippet().getResourceId().getVideoId();
							artistaVideo=item1.getSnippet().getTitle();						
							nombreCanal=item1.getSnippet().getChannelTitle();
							fechaPublicacion=item1.getSnippet().getPublishedAt();
							String sTextoBuscado = "T";
							fechaPublicacion = fechaPublicacion.substring(0,fechaPublicacion.indexOf( sTextoBuscado));
							
							System.out.println("**************** ARTISTA ********************");
							System.out.println("Artista: " + artista);
							System.out.println("T�tulo: " + titulo);
							System.out.println("Artista(s)-T�tulo: " + artistaVideo);
							System.out.println("Canal: " + nombreCanal);
							System.out.println("Fecha Publicaci�n: " + fechaPublicacion);
							System.out.println("Descripcion :" + item1.getSnippet().getDescription());
							System.out.println("Id videoYoutube: " + item1.getSnippet().getResourceId().getVideoId());
							// FM
							// Si hay varios artistas los creamos
							List<Artista> miLista = new ArrayList<>();
							for (int j = 0; j < artistas.length; j++) {
								String unicoArtista = artistas[j].trim().trim();
								unicoArtista = unicoArtista.replaceAll("^\\s*", "").replace("|", "").replace("&", ",");
								String artistaURL = unicoArtista.replace(" ", "%20");
								WebTarget target2 = client.target(getBaseURI2(artistaURL));
								Mensaje2 mensaje2 = target2.request().accept(MediaType.APPLICATION_JSON)
										.get(Mensaje2.class);
								// SI ya existe en nuestra base de datos le saltamos
								Artista miArtista = artistaRepo.findByNombreArtista(unicoArtista);
								if (miArtista == null) {
									miArtista = new Artista();
									miArtista.setNombreArtista(unicoArtista);

									if (mensaje2.getArtist() != null) {
										List<Imagen> imagenes = mensaje2.getArtist().getImage();
										System.out.println("URL imagen: " + imagenes.get(4).getText());
										System.out
												.println("Descripci�n: " + mensaje2.getArtist().getBio().getContent());
										//miArtista.setDescripcion(mensaje2.getArtist().getBio().getContent().trim());
										miArtista.setDescripcion(mensaje2.getArtist().getBio().getSummary().trim());
										miArtista.setUrlImagen(imagenes.get(4).getText());
									}
									numArtistas++;
								}
								System.out.println("**************** FIN ARTISTA ****************");
								miArtista.setNumVideos(miArtista.getNumVideos() + 1);
								artistaRepo.save(miArtista);
								miLista.add(miArtista);
							}
							// Guardamos video a�adiendo la lista de artistas
							Video miVideo = new Video();
							miVideo.setArtistas(miLista);
							miVideo.setNombreVideo(titulo);
							miVideo.setUrlVideo(id);
							miVideo.setUrlImagen("http://img.youtube.com/vi/" + id + "/0.jpg");
							miVideo.setDescripcion(item1.getSnippet().getDescription());
							miVideo.setArtistaVideo(artistaVideo);
							miVideo.setNombreCanal(nombreCanal);
							miVideo.setFechaPublicacion(fechaPublicacion);
							videoRepo.save(miVideo);
							numVideos++;
							System.out.println("**************** FIN VIDEO *****************");
						} else {
							System.out.println("Video ya guardado");
							System.out.println("*************** FIN VIDEO ******************");
						}
					} else {
						System.out.println("No cumple artista-video");
					}

				}
				// Buscamos la pr�xima p�gina de resultados si la hay
				target = client.target(getBaseURI3(mensaje.getNextPageToken()));
				mensaje = target.request().accept(MediaType.APPLICATION_JSON).get(Mensaje3.class);
				
				} while (mensaje.getNextPageToken() != null); 
//			catch (Exception e) {
//				System.out.println("ALGUNO FALLAN POR QUE SI");
//			}

		System.out.println("Artistas totales: " + numArtistas);
		System.out.println("Videos totales: " + numVideos);
		return "redirect:/login.do";
	}
	/*
	 * Esta es la URL del API de Youtube para la busqueda desde una playlist
	 * playlistId = id de una lista de Youtube
	 * maxResults = resultados m�ximos por p�gina entre 1 y 50
	 * part = snipped porque solo queremos esa parte de el json
	 * key = nuestra clave de desarrollador
	 * */
	private static URI getBaseURI() {
		return UriBuilder
				.fromUri(
						"https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=PLVTf1fZL9Ni0N3b0PlX-C4MKpp7Je2-pw&key=AIzaSyA2M9mcPvHWUR06I8xC09Sxn9UGFhhF47c&maxResults=50")
				.build();
	}
	/*
	 * Esta es la URL del API de LastFm para la b�squeda de artista
	 * lang=es es para que nos devuelva la info en espa�ol
	 * api_key es nuestra clave de desarrollador
	 */
	private static URI getBaseURI2(String nombre) {
		return UriBuilder.fromUri("http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=" + nombre
				+ "&api_key=c9d40ab26b8f9dc1ea572755d782bb92&format=json&lang=es").build();
	}
	/*
	 * Esta es la URL del API de Youtube pero para pasar de p�gina 
	 * despu�s de haber hecho una b�squeda, el pageToken es un valor que 
	 * nos devulve tanto la primera b�squeda como esta, y es la p�gina siguiente
	 * si no exite no hay m�s resultados
	 */
	private static URI getBaseURI3(String url) {
		return UriBuilder
				.fromUri("https://www.googleapis.com/youtube/v3/playlistItems?pageToken=" + url
						+ "&part=snippet&playlistId=PLVTf1fZL9Ni0N3b0PlX-C4MKpp7Je2-pw&key=AIzaSyA2M9mcPvHWUR06I8xC09Sxn9UGFhhF47c&maxResults=50")
				.build();
	}

}
