package controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import entidades.Artista;
import entidades.Busqueda;
import entidades.Lista;
import entidades.Usuario;
import entidades.Video;
import modelo.negocio.GestionArtista;
import modelo.negocio.GestionLista;
import modelo.negocio.GestionVideo;



@Controller
public class PlayListController {
	
	@Autowired
	GestionLista gestion;
	@Autowired
	GestionVideo gv;
	@Autowired
	GestionArtista ga;
	
	
	@RequestMapping("/lista_reproduccion.do")
	public String rellenarFormularioBusquedadesdeLista(Model modelo) {
		modelo.addAttribute("busqueda", new Busqueda());
		System.out.println("Lista reproduccion request GET");
		return "lista_reproduccion";
	}
	
	@RequestMapping("/busqueda.do")
	public String rellenarFormularioBusqueda(Model modelo) {
		return "redirect:/lista_reproduccion.do";
	}
	
	
	
	@PostMapping("/busqueda.do")
	public String busqueda(@ModelAttribute("busqueda")String busqueda, Model modelo){
		List<Video> video1;
		List<Artista> artista;
		System.out.println("PASAMOS A BUSCAR");
		video1 = gv.recuperarVideoPorBusqueda(busqueda);
		artista = ga.recuperarArtistaPorBusqueda(busqueda);
		modelo.addAttribute("videos",video1);
		Busqueda nueva = new Busqueda();
		modelo.addAttribute("busqueda", nueva);
		modelo.addAttribute("artista",artista);
		//session.setAttribute("videos", video1);
		return "lista_reproduccion";
	}
	
//	@RequestMapping("/lista_reproduccion.do")
//	public String iraLista(Model modelo) {
//		modelo.addAttribute("busqueda", new Busqueda());
//		System.out.println("Lista reproduccion request GET");
//		return "redirect:/busqueda.do";
//	}
	
	
//	@RequestMapping("/crearPlayList.do")
//	public String crearPlayList(@ModelAttribute("lista") Lista lista, Usuario user){
//	    gestion.crearLista(lista,user);	
//		return "lista_reproduccion";
//		}
//	
//	@RequestMapping("/eliminarPlayList.do")
//	public String eliminarPlayList(@ModelAttribute("lista") Lista lista){
//	gestion.eliminarLista(lista.getIdLista());	
//		return "";
//		
//		}
//	@RequestMapping("/agregarVideoAPlayList")
//	public String agregarVideoAPlayList(@ModelAttribute("lista") Lista lista, Video video){
//		gestion.agregarVideo(lista, video.getIdVideo());
//		return "";
//	}
//	@RequestMapping("/quitarVideoAPlayList")
//	public String quitarVideoAPlayList(@ModelAttribute("lista") Lista lista, Video video){
//		gestion.quitarVideo(lista, video.getIdVideo());
//		return "";
//	}
	}