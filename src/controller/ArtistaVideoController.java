package controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import entidades.Artista;
import entidades.Usuario;
import entidades.Video;
import modelo.negocio.GestionUsuario;
import modelo.negocio.GestionVideo;
import modelo.negocio.GestionArtista;

@Controller
@SessionAttributes(names = { "usuario" })
public class ArtistaVideoController {

	@Autowired
	GestionUsuario gestion;

	@Autowired
	GestionArtista gestionArtista;

	@Autowired
	GestionVideo gv;

	/******************* Controller Recuperar ID**************************** */

	// @RequestMapping(value = "/detalleArtista.do")
	// public String recuperarIdArtista(@ModelAttribute("usuario") Usuario
	// usuario, Artista artista) {
	// gestionArtista.recuperarArtista(artista.getIdArtista());
	// return "redirect:/artista_videos.do";
	// }

	@RequestMapping("/detalleArtista.do")
	public String detalleArtista(@RequestParam("idArtista") Integer id, HttpSession session) {
		Artista nuevo = gestionArtista.recuperarArtista(id);
		session.setAttribute("artista", nuevo);
		return "artista_videos";

	}

	@RequestMapping("reproduccion_videos_artista.do")
	public String reproduccionVideosArtista(@RequestParam("idArtista") Integer id,
			@RequestParam("urlVideo") String idUrl, HttpSession session, Model modelo) {
		Video videoRecogido = gv.recuperarVideoPorURL(idUrl);
		session.setAttribute("videoRecogido", videoRecogido);
		Artista recuperado = gestionArtista.recuperarArtista(id);
		List<Video> videos = gestionArtista.recuperarVideosArtista(recuperado);
		String urlVideos = new String();
		int pasa = 1;
		for (int i = 0; i < videos.size(); i++) {
			if (!videos.get(i).getUrlVideo().equals(idUrl)) {
				if (pasa == 1) {
					urlVideos = videos.get(i).getUrlVideo();
					pasa = 0;
				} else {
					urlVideos += "," + videos.get(i).getUrlVideo();
				}
			}
		}
		modelo.addAttribute("urlVideos", urlVideos);
		return "reproduccion_videos";
	}

	//
	//
	//
	//
	//
	// @RequestMapping(value = "/detalleArtista.do")
	// public String videosArtista(@ModelAttribute("usuario") Usuario usuario,
	// Artista artista) {
	// gestionArtista.recuperarVideosArtista((Artista)
	// artista.getListaCanciones());
	// return "redirect:/artista_videos.do";
	// }

	public GestionUsuario getGestion() {
		return gestion;
	}

	public void setGestion(GestionUsuario gestion) {
		this.gestion = gestion;
	}

}
