package controller;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import aplicacion.json.lastfm.Imagen;
import aplicacion.json.lastfm.Mensaje2;
import aplicacion.json.youtube.Items;
import aplicacion.json.youtube.Mensaje;
import entidades.Artista;
import entidades.Video;
import modelo.dao.ArtistaRepo;
import modelo.dao.VideoRepo;

@Controller
public class RobarController {
	// Trabajando con SessionAttributes

	@Autowired
	ArtistaRepo artistaRepo;
	@Autowired
	VideoRepo videoRepo;

	@RequestMapping("/robarVideos.do")
	public String login() {
		// Para estad�stica de videos y artistas recuperados
		int numVideos = 0;
		int numArtistas = 0;
		int maxRondas = 10; // N�mero de meses a recorrer para la captura

		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);

		LocalDateTime fechaHoy = LocalDateTime.now();
		LocalDateTime fechaI = fechaHoy.plusMonths(-1);
		LocalDateTime fechaF = fechaHoy;
		for (int m = maxRondas; m > 1; m--) {
			/*********************************************************************
			 * La API de Youtube neceista un formato de fechas espec�fico que es
			 * el siguiente: yyyy-MM-ddTHH:mm:ssz de ah� que tenga que hacer los
			 * replace, para quitar el espacio por T y a�adir al final la z
			 *********************************************************************/
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			WebTarget target = client.target(getBaseURI(
					(fechaI.format(formatter).replace(" ", "T") + "z").toString().replace(" ", "T").replace(":", "%3A"),
					(fechaF.format(formatter).replace(" ", "T") + "z").toString().replace(" ", "T").replace(":",
							"%3A")));
			Mensaje mensaje = target.request().accept(MediaType.APPLICATION_JSON).get(Mensaje.class);

			Items item1 = new Items();
			try {

				for (int i = 0; i < mensaje.getItems().size() - 1; i++) {
					item1 = mensaje.getItems().get(i);
					String[] cadena;
					String artista;
					String titulo;
					String id;
					// Como queremos Artist - Nombre video separamos la cadena
					// que nos envia la API
					cadena = item1.getSnippet().getTitle().split("-");
					// Si no tiene - no cumple con nuestro prop�sito y lo
					// saltamos
					if (cadena.length > 1) {
						// Comprobamos si ya exite el video en la bbdd
						if (videoRepo.findByUrlVideo(item1.getId().getVideoId()) == null) {
							artista = cadena[0];
							artista = artista.replace("\"", "").replace("?", "").trim();
							String[] artistas = artista.split(",");
							titulo = cadena[1].trim();
							id = item1.getId().getVideoId();
							System.out.println("***************** VIDEO ********************");
							System.out.println("Artista: " + artista);
							System.out.println("T�tulo: " + titulo);
							System.out.println("Descripcion :" + item1.getSnippet().getDescription());
							System.out.println("Id videoYoutube: " + item1.getId().getVideoId());
							// FM
							// Si hay varios artistas los creamos
							List<Artista> miLista = new ArrayList<>();
							for (int j = 0; j < artistas.length; j++) {
								String unicoArtista = artistas[j].trim().trim();
								unicoArtista = unicoArtista.replaceAll("^\\s*", "").replace("|", "").replace("&", ",");
								String artistaURL = unicoArtista.replace(" ", "%20");
								WebTarget target2 = client.target(getBaseURI2(artistaURL));
								Mensaje2 mensaje2 = target2.request().accept(MediaType.APPLICATION_JSON)
										.get(Mensaje2.class);
								Artista miArtista = artistaRepo.findByNombreArtista(unicoArtista);
								if (miArtista == null) {
									miArtista = new Artista();
									miArtista.setNombreArtista(unicoArtista);

									if (mensaje2.getArtist() != null) {
										List<Imagen> imagenes = mensaje2.getArtist().getImage();
										System.out.println("**************** ARTISTA ********************");
										System.out.println("URL imagen: " + imagenes.get(4).getText());
										System.out
												.println("Descripci�n: " + mensaje2.getArtist().getBio().getContent());
										System.out.println("**************** FIN ARTISTA ****************");
										miArtista.setDescripcion(mensaje2.getArtist().getBio().getContent().trim());
										miArtista.setUrlImagen(imagenes.get(4).getText());
									}
									numArtistas++;
								}
								miArtista.setNumVideos(miArtista.getNumVideos() + 1);
								artistaRepo.save(miArtista);
								miLista.add(miArtista);
								System.out.println("*************** FIN VIDEO ******************");
							}
							// Guardamos video
							Video miVideo = new Video();
							miVideo.setArtistas(miLista);
							miVideo.setNombreVideo(titulo);
							miVideo.setUrlVideo(id);
							miVideo.setUrlImagen("http://img.youtube.com/vi/" + id + "/0.jpg");
							miVideo.setDescripcion(item1.getSnippet().getDescription());
							videoRepo.save(miVideo);
							numVideos++;
							System.out.println("**************** FIN VIDEO *****************");

						} else {
							System.out.println("Falta artista y lo salto");
						}
					} else {
						System.out.println("Video ya en base de datos");
					}
				}
				fechaF = fechaI;
				fechaI = fechaI.plusMonths(-m);

			} catch (Exception e) {
				System.out.println("ALGUNO FALLAN POR QUE SI");
			}
		}
		System.out.println("Artistas totales: " + numArtistas);
		System.out.println("Videos totales: " + numVideos);
		return "redirect:/login.do";
	}

	private static URI getBaseURI(String fechaInicio, String fechaFin) {
		return UriBuilder
				.fromUri(
						"https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&order=rating&publishedAfter="
								+ fechaInicio + "&publishedBefore=" + fechaFin
								+ "&q=VEVO+(OFFICIAL%2BVIDEO)&fields=items(id%2FvideoId%2Csnippet(description%2Ctitle))&key=AIzaSyA2M9mcPvHWUR06I8xC09Sxn9UGFhhF47c")
				.build();
	}

	private static URI getBaseURI2(String nombre) {
		return UriBuilder.fromUri("http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=" + nombre
				+ "&api_key=c9d40ab26b8f9dc1ea572755d782bb92&format=json&lang=es").build();
	}

}
