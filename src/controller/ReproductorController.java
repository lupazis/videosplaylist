package controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import entidades.Artista;
import entidades.Usuario;
import entidades.Video;
import modelo.negocio.GestionArtista;
import modelo.negocio.GestionUsuario;
import modelo.negocio.GestionVideo;

@Controller
public class ReproductorController {
	
	@Autowired
	GestionUsuario gestion;
	
	//Pruebas
	@Autowired
	GestionArtista ga;
	@Autowired
	GestionVideo gv;
	
	
	
	/******************* Controller Reproducir Video**************************** */


	@RequestMapping("/reproduccion_videos.do")
	public String reproductorVideo(@RequestParam("urlVideo") String id,HttpSession session,Model model) {	
		Video videoRecogido=gv.recuperarVideoPorURL(id);
		//List<Video> videos = gv.recuperarVideos();
		session.setAttribute("videoRecogido", videoRecogido);
		model.addAttribute("video", videoRecogido); //Por eliminar lo mismo que en el Request
		return "reproduccion_videos";
	}
	


}
