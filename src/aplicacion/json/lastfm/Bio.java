package aplicacion.json.lastfm;

public class Bio {

	// Para tener solo resumen
	private String content;
	private String summary;
	
	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}
