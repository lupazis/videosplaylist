package aplicacion.json.lastfm;

import java.util.List;

public class Artist {

	private String name;
	private List<Imagen> image;
	private Bio bio;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Imagen> getImage() {
		return image;
	}
	public void setImage(List<Imagen> image) {
		this.image = image;
	}
	public Bio getBio() {
		return bio;
	}
	public void setBio(Bio bio) {
		this.bio = bio;
	}

	
	
}
