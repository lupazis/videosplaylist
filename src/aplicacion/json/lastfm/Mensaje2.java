package aplicacion.json.lastfm;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Mensaje2 {
	private Artist artist;
	

	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}
	
}
