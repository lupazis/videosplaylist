package aplicacion.json.youtube;




public class Snippet2 {
	private String title;
	private String description;
	private ResourceId resourceId;
	private String channelTitle;
	private String publishedAt;
	
	
	
	public String getChannelTitle() {
		return channelTitle;
	}
	public void setChannelTitle(String channelTitle) {
		this.channelTitle = channelTitle;
	}
	public String getPublishedAt() {
		return publishedAt;
	}
	public void setPublishedAt(String publishedAt) {
		this.publishedAt = publishedAt;
	}
	public ResourceId getResourceId() {
		return resourceId;
	}
	public void setResourceId(ResourceId resourceId) {
		this.resourceId = resourceId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
