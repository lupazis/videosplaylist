package aplicacion.json.youtube;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Mensaje3 {
	private List<Items2> items;
	private String nextPageToken;

	public List<Items2> getItems() {
		return items;
	}

	public void setItems(List<Items2> items) {
		this.items = items;
	}

	public String getNextPageToken() {
		return nextPageToken;
	}

	public void setNextPageToken(String nextPageToken) {
		this.nextPageToken = nextPageToken;
	}

	
	
	
}
