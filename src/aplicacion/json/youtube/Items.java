package aplicacion.json.youtube;

public class Items {
	private Id id;
	private Snippet snippet;

	public Snippet getSnippet() {
		return snippet;
	}

	public void setSnippet(Snippet snippet) {
		this.snippet = snippet;
	}

	public Id getId() {
		return id;
	}

	public void setId(Id id) {
		this.id = id;
	}

	
	
	
}
