package azar;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class AzarVideos {
  public static void main (String[] args) {
    int[] ndigitos = new int[12];
    int n;
    
    Random random = new Random();

 // Conjunto de videos ya usados
    Set<Integer> alreadyUsedNumbers = new HashSet<>();

    // Vamos a generar 12 n�meros aleatorios sin repetici�n
    while (alreadyUsedNumbers.size()<12) {

       // N�mero aleatorio entre 0 y 509, excluido el 510.  
       int randomNumber = random.nextInt(510);

       // Si no lo hemos usado ya, lo usamos y lo metemos en el conjunto de usados.
       if (!alreadyUsedNumbers.contains(randomNumber)){
          System.out.println("video no repetido "+randomNumber);
          alreadyUsedNumbers.add(randomNumber);
       }
    }
}
  }