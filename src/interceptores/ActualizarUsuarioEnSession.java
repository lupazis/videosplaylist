package interceptores;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import entidades.Usuario;
import modelo.negocio.GestionUsuario;

public class ActualizarUsuarioEnSession implements HandlerInterceptor {

	GestionUsuario gestionUsuarios;

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handle, ModelAndView mav)
			throws Exception {
		if (mav.getViewName().equals("index")) {
			System.out.println("Voy a Actualizar el usuario de session");
			Usuario viejo = (Usuario) request.getSession().getAttribute("usuario");
			Usuario user = gestionUsuarios.recuperarUsuario(viejo.getEmail());
			request.getSession().setAttribute("usuario", user);
		}

	}

	@Override
	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2) throws Exception {
		// TODO Auto-generated method stub
		return true;
	}

	public GestionUsuario getGestionUsuarios() {
		return gestionUsuarios;
	}

	public void setGestionUsuarios(GestionUsuario gestionUsuarios) {
		this.gestionUsuarios = gestionUsuarios;
	}

}
