package modelo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import entidades.Video;

public interface VideoRepo extends CrudRepository<Video, Integer> {

	Video findByUrlVideo(String urlVideo);
	@Query("select v from Video v where v.nombreVideo LIKE %:busqueda%")
	List<Video> findByBusqueda(@Param("busqueda") String busqueda);

}
