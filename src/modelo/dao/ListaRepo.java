package modelo.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import entidades.Lista;
import entidades.Usuario;


public interface ListaRepo extends CrudRepository<Lista, Integer> {
	
    }
