package modelo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import entidades.Artista;

/** Repositorio de spring CrudRepository.
 * con el podemos hacer b�squedas f�ciles.
 * */
public interface ArtistaRepo extends CrudRepository<Artista, Integer> {

	Artista findByNombreArtista(String nombreArtista);
	List<Artista> findFirst10ByOrderByNombreArtista();
	List<Artista> findFirst12ByOrderByNumVideosDesc();
	@Query("select a from Artista a where a.nombreArtista LIKE %:busqueda%")
	List<Artista> findByBusqueda(@Param("busqueda") String busqueda);

}
