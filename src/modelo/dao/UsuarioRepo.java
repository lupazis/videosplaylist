package modelo.dao;

import org.springframework.data.repository.CrudRepository;

import entidades.Usuario;

public interface UsuarioRepo extends CrudRepository<Usuario, Integer> {

	Usuario findByNombreUsuario(String nombre);

	Usuario findByEmail(String email);


}
