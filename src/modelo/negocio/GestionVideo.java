package modelo.negocio;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import entidades.Video;
import modelo.dao.VideoRepo;



@Service
public class GestionVideo {
	
	@Autowired
	VideoRepo repo;
	
	
	public Video recuperarVideo(Integer idVideo){		
		return repo.findOne(idVideo);
	}
	
	public List<Video> recuperarVideos(){		
		return (List<Video>) repo.findAll();
	}

	public Video recuperarVideoPorURL(String id) {
		return (Video) repo.findByUrlVideo(id);
	}

	public List<Video> recuperarVideoPorBusqueda(String busqueda) { 
		return (List<Video>) repo.findByBusqueda(busqueda);
	}
	
}
