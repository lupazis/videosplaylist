package modelo.negocio;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import entidades.Usuario;
import modelo.dao.ArtistaRepo;
import modelo.dao.ListaRepo;
import modelo.dao.UsuarioRepo;

@Service
public class GestionUsuario {

	@Autowired
	UsuarioRepo repoU;
	@Autowired
	ArtistaRepo repoA;
	@Autowired
	ListaRepo repoL;

	@Transactional
	public boolean registrarUsuario(Usuario user) {
		if (repoU.findByEmail(user.getEmail()) == null) {
			repoU.save(user);
			return true;
		} else
			return false;
	}

	public Usuario logarUsuario(Usuario user) {
		Usuario userRecogido = repoU.findByEmail(user.getEmail());
		if (userRecogido != null && user.getPass().equals(userRecogido.getPass()))
			return userRecogido;
		else
			return null;
	}

	@Transactional
	public boolean aniadirArtista(Usuario user, Integer idArtista) {
		user.addArtistasFavoritos(repoA.findOne(idArtista));
		repoU.save(user);
		return true;
	}

	@Transactional
	public boolean eliminarArtista(Usuario user, Integer idArtista) {
		user.removeArtistaFavorito(repoA.findOne(idArtista));
		repoU.save(user);
		return true;
	}

	public Usuario recuperarUsuario(String email) {
		return repoU.findByEmail(email);
	}

	////// *******SSSandGGGG*****//////
	public UsuarioRepo getRepoU() {
		return repoU;
	}

	public void setRepoU(UsuarioRepo repoU) {
		this.repoU = repoU;
	}

	public ArtistaRepo getRepoA() {
		return repoA;
	}

	public void setRepoA(ArtistaRepo repoA) {
		this.repoA = repoA;
	}

	public ListaRepo getRepoL() {
		return repoL;
	}

	public void setRepoL(ListaRepo repoL) {
		this.repoL = repoL;
	}

}
