package modelo.negocio;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entidades.Artista;

import entidades.Video;
import modelo.dao.ArtistaRepo;

@Service
@Transactional
public class GestionArtista {
	
	@Autowired
	ArtistaRepo repo;
	

	public Artista recuperarArtista(Integer idArtista){
		return repo.findOne(idArtista);	
	}
	
	public List<Video> recuperarVideosArtista(Artista a){
		return a.getListaCanciones();
	}
	
	public void guardarArtista(Artista a){
		repo.save(a);	
	}
	
	public ArtistaRepo getRepo() {
		return repo;
	}
	public void setRepo(ArtistaRepo repo) {
		this.repo = repo;
	}

	public List<Artista> recuperarArtistas() {
		return (List<Artista>) repo.findAll();
	}
	
	public List<Artista> recuperar12mejoresArtistas() {
		return (List<Artista>) repo.findFirst12ByOrderByNumVideosDesc();
	}

	public List<Artista> recuperarArtistaPorBusqueda(String busqueda) {

		return (List<Artista>) repo.findByBusqueda(busqueda);
	}

}
