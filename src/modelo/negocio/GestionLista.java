package modelo.negocio;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entidades.Lista;
import entidades.Usuario;
import entidades.Video;
import modelo.dao.ListaRepo;
import modelo.dao.VideoRepo;

@Service
@Transactional
public class GestionLista {

	@Autowired
	ListaRepo repoL;
	@Autowired
	VideoRepo repoV;
	
	public void crearLista(Lista lista,Usuario user) {
		user.addLista(repoL.save(lista));		
	}
	
	public void eliminarLista(int id) {
		repoL.delete(id);
	}
	
	public void agregarVideo(Lista lista, Integer idVideo){
		lista.add(repoV.findOne(idVideo));
		repoL.save(lista);
	}
	
	public void quitarVideo(Lista lista, Integer idVideo){
		lista.remove(repoV.findOne(idVideo));
		repoL.save(lista);
	}
	
	public Lista recuperarLista(Integer idLista){
		return repoL.findOne(idLista);
		
	}
	
// *******************///****************//
	public ListaRepo getRepository() {
		return repoL;
	}

	public void setRepository(ListaRepo repository) {
		this.repoL = repository;
	}

}
