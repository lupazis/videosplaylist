package entidades;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
//import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Usuario {

	@Id
	@GeneratedValue
	Integer idUsuario;
	String nombreUsuario;
	@Column(unique = true)
	String email;
	String pass;
	@ManyToMany
	List<Artista> artistasFavoritos;
	@OneToMany
	List<Lista> playLists;

	public Usuario() {
		super();
	}

	public Artista remove(int arg0) {
		return artistasFavoritos.remove(arg0);
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public List<Artista> getArtistasFavoritos() {
		return artistasFavoritos;
	}

	public void setArtistasFavoritos(List<Artista> artistasFavoritos) {
		this.artistasFavoritos = artistasFavoritos;
	}

	public List<Lista> getPlayList() {
		return playLists;
	}

	public void setPlayList(List<Lista> playLists) {
		this.playLists = playLists;
	}

	public void addArtistasFavoritos(Artista element) {
		artistasFavoritos.add(element);
	}

	public boolean removeArtistaFavorito(Object o) {
		return artistasFavoritos.remove(o);

	}

	public boolean addLista(Lista e) {
		return playLists.add(e);
	}

	public Lista removeLista(int index) {
		return playLists.remove(index);
	}

}
