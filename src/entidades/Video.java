package entidades;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;




@Entity
public class Video {
	@Id
	@GeneratedValue
	Integer idVideo;
	String nombreVideo;
	String artistaVideo;
	@ManyToMany(fetch=FetchType.EAGER)
	List<Artista> artistas;
	@Column(columnDefinition = "text")
	String descripcion;
	String nombreCanal;	
	String fechaPublicacion;
	String urlVideo;
	String urlImagen;
	
	
	public Video() {
		super();
	}


	

	public String getArtistaVideo() {
		return artistaVideo;
	}




	public void setArtistaVideo(String artistaVideo) {
		this.artistaVideo = artistaVideo;
	}




	public String getNombreCanal() {
		return nombreCanal;
	}




	public void setNombreCanal(String nombreCanal) {
		this.nombreCanal = nombreCanal;
	}




	public String getFechaPublicacion() {
		return fechaPublicacion;
	}




	public void setFechaPublicacion(String fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}




	public Integer getIdVideo() {
		return idVideo;
	}


	public void setIdVideo(Integer idVideo) {
		this.idVideo = idVideo;
	}


	public String getNombreVideo() {
		return nombreVideo;
	}


	public void setNombreVideo(String nombreVideo) {
		this.nombreVideo = nombreVideo;
	}


	public List<Artista> getArtistas() {
		return artistas;
	}


	public void setArtistas(List<Artista> artistas) {
		this.artistas = artistas;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getUrlVideo() {
		return urlVideo;
	}


	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}


	public String getUrlImagen() {
		return urlImagen;
	}


	public void setUrlImagen(String urlImagen) {
		this.urlImagen = urlImagen;
	}
	
}
