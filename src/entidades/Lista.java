package entidades;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Lista {
	@Id
	@GeneratedValue
	Integer idLista;
	String nombreLista;
	@OneToMany(fetch=FetchType.EAGER)
	List<Video> videos;
		
	public Lista() {
		super();
	}


	public Integer getIdLista() {
		return idLista;
	}

	public void setIdLista(Integer idLista) {
		this.idLista = idLista;
	}

	public String getNombreLista() {
		return nombreLista;
	}

	public void setNombreLista(String nombreLista) {
		this.nombreLista = nombreLista;
	}

	public List<Video> getListaVideos() {
		return videos;
	}

	public void setListaVideos(List<Video> listaVideos) {
		this.videos = listaVideos;
	}


	public boolean add(Video arg0) {
		return videos.add(arg0);
	}


	public boolean remove(Object arg0) {
		return videos.remove(arg0);
	}		
	
	
	
}
