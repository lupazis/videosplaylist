package entidades;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Artista {
	@Column(unique=true)
	String nombreArtista;
	@Id
	@GeneratedValue
	Integer idArtista;
	@ManyToMany(mappedBy = "artistas", fetch=FetchType.EAGER)
	List<Video> listaCanciones;
	@Column(columnDefinition = "text")
	String descripcion;
	String urlImagen;
	int numVideos;

	public int getNumVideos() {
		return numVideos;
	}

	public void setNumVideos(int numVideos) {
		this.numVideos = numVideos;
	}

	public Artista() {
		super();
	}

	public String getUrlImagen() {
		return urlImagen;
	}

	public void setUrlImagen(String urlImagen) {
		this.urlImagen = urlImagen;
	}

	public Integer getIdArtista() {
		return idArtista;
	}

	public void setIdArtista(Integer idArtista) {
		this.idArtista = idArtista;
	}

	public String getNombreArtista() {
		return nombreArtista;
	}

	public void setNombreArtista(String nombreArtista) {
		this.nombreArtista = nombreArtista;
	}

	public List<Video> getListaCanciones() {
		return listaCanciones;
	}

	public void setListaCanciones(List<Video> listaCanciones) {
		this.listaCanciones = listaCanciones;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
