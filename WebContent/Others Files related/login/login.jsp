<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
  	<head>
		<title>VevoPlayList Stolen Videos</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="icon" type="image/png" href="images/video_icon.png">
		<link href="css/login.css" rel="stylesheet">		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/login.js"></script>
	</head>
    <body>
<div id="videoDiv"> 
<div id="videoBlock"> 
<div><img src="images/bg_color.jpg" id="videosubstitute" alt="" width="800"></div>
<video preload="preload" id="video" autoplay="autoplay" loop="loop">
<source src="videos/video-background.webm" type="video/webm">
<source src="videos/video-background.mp4" type="video/mp4">
</video> 
<div id="videoMessage">

        <form:form modelAttribute="usuario">
            <div class="imgcontainer">
                <img src="images/logo_arigraphs.png" alt="Avatar" class="avatar">
            </div>
			<form:errors path="email" cssClass="error"></form:errors>
            <div class="container">                
                <form:input class="text_box" placeholder="Introduce tu email" path="email" required="true"></form:input>               
                <form:password class="text_box" placeholder="Introduce tu contrase�a" path="pass" required="true"></form:password>

                <button type="submit">Login</button>
                <input type="checkbox" checked="checked"> No cerrar sesi�n
            </div>

            <div class="container" >
                 <button type="button" onclick="location.href = 'login.do'" class="cancelbtn" >Home</button>
                <span class="psw">�No tienes una cuenta? <a href="registrarUsuario.do">Reg�strate</a></span>
            </div>
        </form:form>
        <h2>VevoPlayList</h2>

</div>
</div>
</div>
    </body>
</html>
