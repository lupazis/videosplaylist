
      // 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: 400 ,    // Lo comentamos porque ya lo definimos en el iframe arriba		
          width: 800,
          videoId: 'M7lc1UVf-VE',
		  playerVars: {
            color: 'white',
            //playlist: 'sPTn0QEhxds,JGwWNGJdvx8,fJ9rUzIMcZQ,iy4mXZN1Zzk,c18441Eh_WE,PWgvGjAhvIw'
        },
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
      }

  var videos = [
        {
          vid: 'NI_OtJNYpIA',
          startSeconds: 10,
          endSeconds: 15
        },
        {
          vid: '_zXqkI17r4Y',
          startSeconds: 10,
          endSeconds: 15
        },
        {
          vid: 'Rf3DXJwcMiY',
          startSeconds: 10,
          endSeconds: 15
        },
        {
          vid: 'pswiT9-YCJI',
          startSeconds: 10,
          endSeconds: 15
        },
        {
          vid: 'oDJ2IcuM9CY',
          startSeconds: 10,
          endSeconds: 15
        }
      ];
      var index = 0;
		
			
		
		
      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
  event.target.cueVideoById({
          videoId: videos[index].vid,
          startSeconds: videos[index].startSeconds,
          endSeconds: videos[index].endSeconds
        });
        event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      
      function onPlayerStateChange(event) {
      console.log("State change: " + event.data + " for index: " + index);

        if (event.data === YT.PlayerState.ENDED && player.getVideoLoadedFraction() > 0) {
          console.log(index);
          if (index < videos.length - 1) {
            index++;
            event.target.loadVideoById({
              videoId: videos[index].vid,
              startSeconds: videos[index].startSeconds,
              endSeconds: videos[index].endSeconds
            });
          }
        }
      }
      function stopVideo() {
        player.stopVideo();
      }
   