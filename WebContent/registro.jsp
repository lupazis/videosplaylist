<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<title>VevoPlayList Stolen Videos</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" type="image/png" href="images/video_icon.png">
<link rel="stylesheet" href="css/login.css" type="text/css">

</head>
<body>


	<form:form modelAttribute="usuario">
		<div class="imgcontainer">
			<img src="images/logo_arigraphs.png" alt="Avatar" class="avatar">
		</div>

		<div class="container">
			<!--<label><b>Usuario</b></label>-->
			<form:input placeholder="Introduce tu usuario" path="nombreUsuario"
				required="true"></form:input>

			<!--<label><b>Contraseņa</b></label>-->
			<form:password placeholder="Introduce tu contraseņa"
				path="pass" required="true"></form:password>

			<!--<label><b>Email</b></label>-->
			<form:errors path="email"></form:errors>
			<form:input placeholder="Introduce tu email" path="email"
				required="true"></form:input>
			<button type="submit">Registrar</button>
		</div>

		<div class="container" style="background-color: #f1f1f1">
			<button type="button" onclick="location.href = 'login.do'"
				class="cancelbtn">Home</button>
		</div>
	</form:form>
</body>
</html>
