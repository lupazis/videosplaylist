<%@page import="org.springframework.web.bind.annotation.ModelAttribute"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true" import="entidades.*"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="modelo.dao.ArtistaRepo"%>
<%@page import="modelo.negocio.GestionArtista"%>
<%@page import="modelo.negocio.GestionVideo,java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<title>VevoPlayList Stolen Videos</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" type="image/png" href="images/video_icon.png">
<link rel="stylesheet" href="css/style.css" type="text/css">
<script type="text/javascript" src="js/index.js"></script>

</head>
<body>
	<%
		Usuario user = (Usuario) session.getAttribute("usuario");
	%>

	<div class="wrapper row1">
		<header id="header" class="clear">
			<div id="hgroup">
				<h1>
					<a href="#">VevoPlayList</a>
				</h1>
				<h2>Disfruta de los v�deos y la m�sica que te gusta</h2>

			</div>
			<nav>
				<ul>
					<li><a href="index.do"><img src="images/biblioteca.png"
							width="20" height="20"></a></li>
					<li><a href="lista_reproduccion.do">Lista de Reproducci�n</a></li>
					<li><a href="lista_favoritos.do">Favoritos</a></li>
					<a href="#"><img src="images/logo-login.png"
						alt="Iniciar Sesi�n" width="30" height="30"></a>
					<a href="#" class="login-user"><%=user.getNombreUsuario()%></a>

				</ul>
			</nav>
		</header>
	</div>

	<!-- content -->
	<div class="wrapper row2">


		<div id="container" class="clear">



			<!-- Content -->
			<div id="homepage">

				<!--<h1 class="h1">B�squeda</h1>-->

				<div class="flexsearch">
					<div class="flexsearch--wrapper">
						<form:form modelAttribute="busqueda" method="post"
							action="busqueda.do" class="flexsearch--form">
							<div class="flexsearch--input-wrapper">
								<form:input class="flexsearch--input" path="busqueda"
									placeholder="t�tulo, artista, g�nero" />
							</div>
							<input class="flexsearch--submit" type="submit" value="&#10140;" />
						</form:form>
					</div>
				</div>

				<div class="clearfix"></div>

				<!-- ########################################################################################## -->
				<section class="clear">
					<br>
					<button onclick="nuevaLista()" class="buttonList buttonRed">Nueva
						Lista Reproducci�n</button>
					<input type="text" id="txt_titulo">
					<button onclick="crearLista()" id="btn_crear"
						class="buttonList buttonRed">Crear</button>
					<button onclick="cancelarLista()" id="btn_cancelar"
						class="buttonList buttonRed">Cancelar</button>

					<div id="lista_reproduccion"></div>

					<div class="clearfix"></div>
					<article class="videos">
						<!--<h4>Resize the browser window to see the effect.</h4>-->
						<br>
						<%
							@SuppressWarnings("unchecked") 
							List<Video> video = (List<Video>) request.getAttribute("videos");
							if (video != null) {
						%>


						<%
							for (int i = 0; i < video.size(); i++) {
						%>
						<div class="responsive">
							<div class="gallery">
								<a target=""
									href="reproduccion_videos.do?urlVideo=<%=video.get(i).getUrlVideo()%>">
									<img src="<%=video.get(i).getUrlImagen()%>"
									alt="<%=video.get(i).getArtistas().get(0).getNombreArtista()%>">
								</a>
								<button onclick="agregarLista()" id="btn_agregar"
									class="buttonAccion buttonRed">Agregar</button>
								<button onclick="eliminarLista()" id="btn_eliminar"
									class="buttonAccion buttonRed">Eliminar</button>
								<div class="desc"><%=video.get(i).getArtistas().get(0).getNombreArtista()%>
									-
									<%=video.get(i).getNombreVideo()%></div>

							</div>
						</div>
						<%
							}
						%>

						<div class="clearfix"></div>

						<!-- ########################################################################################## -->
						<div class="desctext">
							<br>
							<p>Disfruta de los v�deos y la m�sica que te gusta, sube
								material original y comparte el contenido con tus amigos, tu
								familia y el resto del mundo en PlayListVevo.</p>

						</div>

						<!-- ########################################################################################## -->



					</article>
					<!-- ########################################################################################## -->

					<!--<article class="one_third lastbox"></article>-->
				</section>

				<section class="artistas clear">
					<%
						@SuppressWarnings("unchecked") 
						List<Artista> artista = (List<Artista>) request.getAttribute("artista");
							if (artista != null) {
					%>

					<%
						for (int i = 0; i < artista.size(); i++) {
					%>
					<div class="responsive">
						<div class="responsive-item-content">
							<a target=""
								href="detalleArtista.do?idArtista=<%=artista.get(i).getIdArtista()%>">
								<img class="circular" src="<%=artista.get(i).getUrlImagen()%>"
								alt="<%=artista.get(i).getNombreArtista()%>">
							</a> <i class="fa fa-star fa-2x aria"></i>
							<div class="descA"><%=artista.get(i).getNombreArtista()%></div>

						</div>
					</div>
					<%
						}
							}
						}
					%>

					<div class="spacer"></div>

				</section>
				<!-- ########################################################################################## -->
			</div>
			<!-- / content body -->
		</div>
	</div>
	<!-- Footer -->
	<div class="wrapper row3">
		<footer id="footer" class="clear">
			<p class="fl_left">
				Copyright &copy; 2017 - Derechos Reservados - <a href="#">VevoPlayList</a>
			</p>
			<p class="fl_right">
				Dise�ado por <a href="" title="">Videos Stolen</a>
			</p>
		</footer>
	</div>
	<script async type='text/javascript'
		src='https://use.fontawesome.com/493ef35f7a.js'></script>



	<!---------------------------------------- Modal ------------------------------------------------>
	<div id="listasModal" class="modal">

		<!-- Modal content -->
		<div class="modal-content">
			<div class="modal-header">
				<span class="close">&times;</span>
				<h2>Listas de Reproducci�n</h2>
			</div>
			<div class="modal-body">
				<p>Shakira</p>
				<p>Juanes</p>
				<p>Thalia</p>
				<p>Queen</p>
			</div>
			<div class="modal-footer">
				<h3>VevoPlayList</h3>
			</div>
		</div>

	</div>

	<script>
		// Get the modal
		var modal = document.getElementById('listasModal');

		// Get the button that opens the modal
		var btn = document.getElementById("btn_agregar");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

		// When the user clicks the button, open the modal 
		function agregarLista() {
			modal.style.display = "block";
		}

		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
			modal.style.display = "none";
		}

		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
			if (event.target == modal) {
				modal.style.display = "none";
				btn = "";
			}
		}
	</script>
</body>
</html>
