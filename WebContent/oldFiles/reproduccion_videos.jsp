<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" session="true" import="entidades.*"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page	import="modelo.dao.ArtistaRepo,java.util.List,java.util.ArrayList"%>
<%@page import="modelo.negocio.GestionArtista"%>
<%@page import="modelo.negocio.GestionVideo"%>
<!DOCTYPE html>
<html>
<head>
<title>VevoPlayList Stolen Videos</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" type="image/png" href="images/video_icon.png">
<link rel="stylesheet" href="css/style.css" type="text/css">
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js?ver=1.7.1'></script>
<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript" src="js/reproductor.js"></script>
<script src="https://www.youtube.com/iframe_api"></script>

</head>
<body>
	<%
		Video miVideo = (Video) session.getAttribute("videoRecogido");
	%>
	<%-- 	<input id="urlVideo" type="hidden" value="<%=request.getParameter("urlVideo")%>"> --%>
	<input id="urlVideo" type="hidden" value="<%=miVideo.getUrlVideo()%>">
	<!--Prueba para la lista -->
	<input id="playlist" type="hidden" value="<%=request.getAttribute("urlVideos")%>">
	<%
		Usuario user = (Usuario) session.getAttribute("usuario");
	%>
	<div class="wrapper row1">
		<header id="header" class="clear">
			<div id="hgroup">
				<h1>
					<a href="#">VevoPlayList</a>
				</h1>
				<h2>Disfruta de los vídeos y la música que te gusta</h2>

			</div>
			<nav>
				<ul>
					<li><a href="index.do"><img src="images/biblioteca.png"
							width="20" height="20"></a></li>
					<li><a href="lista_reproduccion.do">Lista de Reproducción</a></li>
					<li><a href="lista_favoritos.do">Favoritos</a></li>
					<a href="#"><img src="images/logo-login.png"
						alt="Iniciar Sesión" width="30" height="30"></a>
					<a href="#" class="login-user"><%=user.getNombreUsuario()%></a>

				</ul>
			</nav>
		</header>
	</div>

	<!-- content -->
	<div class="wrapper row2">


		<div id="container" class="clear">



			<!-- Content -->
			<div id="homepage">



				<section class="clear">
					<div class="spacer"></div>
					<div class="clearfix"></div>



					<!-- ########################################################################################## -->
					<section class="artistas clear">

						<!-- 1. The <iframe> (and video player) will replace this <div> tag. -->

						<!--             /* <div id="video-reproductor"></div>*/-->
						<!--              <div class="video-options">The picture above is 350px wide. The total width of this element is also 350px.</div>-->

						<div class="spacer"></div>
						<div id="video-reproductor"></div>
						<!-- Start Video Post -->
						<div class="video-post-wrapper">

							<div class="video-posts-video">
								<!-- Video removido de aqui-->
							</div>
							<div class="video-posts-data">

								<div class="video-post-title">
								<h4><%=miVideo.getArtistaVideo()%>	</h4>
								<div class="spacer"></div>
								
									<span class="video-icons"><i class="fa fa-info-circle"></i></span>
									<div class="video-post-info">
										
										<div class="video-post-date">
											<span><i class="fa fa-calendar"></i></span>
											<p><%=miVideo.getFechaPublicacion()%></p>
											<span class="video-posts-author"> <i
												class="fa fa-folder-o"></i>
												<!-- cambiado por fuera para que no modifique tamaño fa fa -->
											</span> <a href="#"><%=miVideo.getNombreCanal()%></a>																				
										</div>
										<div class="clearfix"></div>
										<hr>
										<div class="btn_Reproductor">
									<button type="submit" class="btn_Suscribe btn_Red">
										<img src="images/video_icon_white.png" width="15" height="15"
											alt="submit" /><span> Suscribirse</span>
									</button>
								</div>	
									</div>
									
								
								</div>
								<div class="video-post-counter">
									<div class="video-post-viewers">
										<h3>2.151.653.266 visualizaciones</h3>
										
									</div>
									<div class="video-like">
										<span><i class="fa fa-thumbs-o-up"></i></span>
										<p>421825</p>
									</div>
									<div class="video-dislike">
										<span><i class="fa fa-thumbs-o-down"></i></span>
										<p>9694</p>

									</div>
								</div>
							</div>
							<!-- Start Tags And Share Options -->
							<div class="tags-and-share video-share">
								<div class="btn_Reproductor">
									<button type="submit" class="btn_Suscribe btn_Red">
										<img src="images/video_icon_white.png" width="15" height="15"
											alt="submit" /><span> Suscribirse</span>
									</button>
								</div>
								<div class="share-options">
									<h4>Compartir en..</h4>
									<ul class="social-share">
										<li class="twitter-bg"><a
											href="https://twitter.com/?status=Me gusta este video https://www.youtube.com/watch?v=<%=miVideo.getUrlVideo()%>"
											target="_blank"><i class="fa fa-twitter"></i></a></li>
										<li class="facebook-bg"><a
											href="https://www.facebook.com/sharer/sharer.php?u=https://www.youtube.com/watch?v=<%=miVideo.getUrlVideo()%>"
											target="_blank"><i class="fa fa-facebook"></i></a></li>
										<li class="google-bg"><a
											href="https://plus.google.com/share?url=https://www.youtube.com/watch?v=<%=miVideo.getUrlVideo()%>"
											target="_blank"><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<!-- End Tags And Share Options  -->
							<div class="video-post-text">
							Publicado <%=miVideo.getFechaPublicacion()%>						
							
								<h4><%=miVideo.getNombreVideo()%></h4>
								<hr>
								<p><a href="" id="mostrar_mas">Mostrar más</a></p>
<div id="detalle_video" style="display:none">
<p class="small"><%=miVideo.getDescripcion()%></p>
<pre><script type="text/javascript">
$(document).ready(function(){ $('#mostrar_mas').toggle( function(e){ $('#detalle_video').slideDown();$(this).text('MOSTRAR MENOS');e.preventDefault();},function(e){ $('#detalle_video').slideUp();$(this).text('MOSTRAR MÁS');e.preventDefault();});});</script></pre>
</div>

							</div>

						</div>

					</section>



					<article class="videos">

						<!--<h4>Resize the browser window to see the effect.</h4>-->
						<br>

						<div class="clearfix"></div>



						<div class="clearfix"></div>







					</article>



				</section>
				<!-- ########################################################################################## -->

				<!-- ########################################################################################## -->
			</div>
			<!-- / content body -->
		</div>
	</div>
	<!-- Footer -->
	<div class="wrapper row3">
		<footer id="footer" class="clear">
			<p class="fl_left">
				Copyright &copy; 2017 - Derechos Reservados - <a href="#">VevoPlayList</a>
			</p>
			<p class="fl_right">
				Diseñado por <a href="" title="">Videos Stolen</a>
			</p>
		</footer>
	</div>
	<script async type='text/javascript'
		src='https://use.fontawesome.com/493ef35f7a.js'></script>
</body>

</html>
