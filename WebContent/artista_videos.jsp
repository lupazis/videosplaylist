<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true" import="entidades.*"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="modelo.dao.ArtistaRepo"%>
<%@page import="modelo.negocio.GestionArtista"%>
<%@page import="modelo.negocio.GestionVideo"%>
<!DOCTYPE html>
<html>
<head>
<title>VevoPlayList Stolen Videos</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" type="image/png" href="images/video_icon.png">
<link rel="stylesheet" href="css/style.css" type="text/css">
<script type="text/javascript" src="js/index.js"></script>

</head>
<body>
	<%
		HttpSession sesion = request.getSession(false);
		if (sesion == null) {
			response.sendRedirect("login.do");
		} else {
			Artista artista = (Artista) session.getAttribute("artista");
			Usuario user = (Usuario) session.getAttribute("usuario");
	%>

	<div class="wrapper row1">
		<header id="header" class="clear">
			<div id="hgroup">
				<h1>
					<a href="#">VevoPlayList</a>
				</h1>
				<h2>Disfruta de los v�deos y la m�sica que te gusta</h2>

			</div>
			<nav>
				<ul>
					<li><a href="index.do"><img src="images/biblioteca.png"
							width="20" height="20"></a></li>
					<li><a href="lista_reproduccion.do">Lista de Reproducci�n</a></li>
					<li><a href="lista_favoritos.do">Favoritos</a></li>
					<a href="#"><img src="images/logo-login.png"
						alt="Iniciar Sesi�n" width="30" height="30"></a>
					<a href="#" class="login-user"><%=user.getNombreUsuario()%></a>

				</ul>
			</nav>
		</header>
	</div>

	<!-- content -->
	<div class="wrapper row2">


		<div id="container" class="clear">



			<!-- Content -->
			<div id="homepage">

				<!--<h1 class="h1">B�squeda</h1>-->

				<div class="flexsearch">
					<div class="flexsearch--wrapper">
						<form class="flexsearch--form" action="#" method="post">
							<div class="flexsearch--input-wrapper">
								<input class="flexsearch--input" type="search"
									placeholder="t�tulo, artista, g�nero">
							</div>
							<input class="flexsearch--submit" type="submit" value="&#10140;" />
						</form>
					</div>
				</div>

				<div class="clearfix"></div>

				<!-- ########################################################################################## -->
				<div class="desctext">
					<br>
					<p>Hoy vas a conocer hechos curiosos de tus bandas y artistas
						favoritos de todos los tiempos. Tal vez te encuentres con cosas
						que ya sab�as, pero seguro que te sorprender�s en m�s de una
						ocasi�n. Ponte el cintur�n y prep�rate para agregar datos in�tiles
						a tu cerebro.</p>
				</div>
				<section class="clear">
					<br> <br> <br> <br>
					<div class="clearfix"></div>

					<!-- ########################################################################################## -->
					<section class="artistas clear">
						<div class="detalle_artista">
							<div class="responsive-item-content">
								<a target="" href=""> <img class="artista_circular"
									src="<%=artista.getUrlImagen()%>"
									alt="<%=artista.getNombreArtista()%>">
								</a> <i class="fa fa-star fa-2x aria"></i>
								<div class="nombreArtista"><%=artista.getNombreArtista()%></div>

							</div>
						</div>


						<div class="clearfix"></div>



						<div class="spacer"></div>

					</section>

					<div class="descArtista">
						<br>
						<p><%=artista.getDescripcion()%></p>

					</div>
					<!-- ########################################################################################## -->


					<article class="videos">

						<!--<h4>Resize the browser window to see the effect.</h4>-->
						<br>
						<%
							for (int i = 0; i < artista.getNumVideos(); i++) {
						%>
						<div class="responsive">
							<div class="gallery">
								<a
									href="reproduccion_videos_artista.do?urlVideo=<%=artista.getListaCanciones().get(i).getUrlVideo()%>&idArtista=<%=artista.getIdArtista()%>">
									<img
									src="<%=artista.getListaCanciones().get(i).getUrlImagen()%>"
									alt="<%=artista.getNombreArtista()%>">
								</a>

								<div class="desc"><%=artista.getNombreArtista()%>
									-
									<%=artista.getListaCanciones().get(i).getNombreVideo()%></div>

							</div>
						</div>


						<%
							}
						%>
						<div class="clearfix"></div>
					</article>



				</section>
				<!-- ########################################################################################## -->

				<!-- ########################################################################################## -->
			</div>
			<!-- / content body -->
		</div>
	</div>
	<!-- Footer -->
	<div class="wrapper row3">
		<footer id="footer" class="clear">
			<p class="fl_left">
				Copyright &copy; 2017 - Derechos Reservados - <a href="#">VevoPlayList</a>
			</p>
			<p class="fl_right">
				Dise�ado por <a href="" title="">Videos Stolen</a>
			</p>
		</footer>
	</div>
	<script async type='text/javascript'
		src='https://use.fontawesome.com/493ef35f7a.js'></script>
	<%
		}
	%>
</body>
</html>
