<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
  	<head>
		<title>VevoPlayList Stolen Videos</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="icon" type="image/png" href="images/video_icon.png">
		<link href="css/login.css" rel="stylesheet">		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/login.js"></script>
	</head>
    <body>
<iframe id="video-background" width="560" height="315" src="//www.youtube.com/embed/nCkpzqqog4k?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1&amp;html5=1&amp;allowfullscreen=true&amp;wmode=transparent&amp;loop=1&amp;playlist=nCkpzqqog4k&amp;volume=0" frameborder="0" allowfullscreen></iframe>
		<div id="formulario">
		        <h2>VevoPlayList</h2>
			<!--<div contenido del formulario</div>-->

        <form:form modelAttribute="usuario" action="index.do">
            <div class="imgcontainer">
                <img src="images/logo_arigraphs.png" alt="Avatar" class="avatar">
            </div>
			<form:errors path="email" cssClass="error"></form:errors>
            <div class="container">                
                <form:input class="text_box" placeholder="Introduce tu email" path="email" required="true"></form:input>               
                <form:password class="text_box" placeholder="Introduce tu contrase�a" path="pass" required="true"></form:password>

                <button type="submit">Login</button>
                <input type="checkbox" checked="checked"> No cerrar sesi�n
            </div>

            <div class="container" >
                 <button type="button" onclick="location.href = 'login.do'" class="cancelbtn" >Home</button>
                <span class="psw">�No tienes una cuenta? <a href="registrarUsuario.do">Reg�strate</a></span>
            </div>
        </form:form>

</div>
    </body>
</html>
