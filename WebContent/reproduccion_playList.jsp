<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true" import="entidades.*"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="modelo.dao.ArtistaRepo"%>
<%@page import="modelo.negocio.GestionArtista"%>
<%@page import="modelo.negocio.GestionVideo"%>
<!DOCTYPE html>
<html>
<head>
<title>VevoPlayList Stolen Videos</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" type="image/png" href="images/video_icon.png">
<link rel="stylesheet" href="css/style.css" type="text/css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript" src="js/reproductor.js"></script>

<script src="https://www.youtube.com/iframe_api"></script>

</head>
<body>
	<%
		Video miVideo = (Video) session.getAttribute("videoRecogido");
	%>
	<%-- 	<input id="urlVideo" type="hidden" value="<%=request.getParameter("urlVideo")%>"> --%>
	<input id="urlVideo" type="hidden" value="<%=miVideo.getUrlVideo()%>">
	<%
	Artista artista = (Artista) session.getAttribute("artista");
		Usuario user = (Usuario) session.getAttribute("usuario");
	
	%>
	<div class="wrapper row1">
		<header id="header" class="clear">
			<div id="hgroup">
				<h1>
					<a href="#">VevoPlayList</a>
				</h1>
				<h2>Disfruta de los v�deos y la m�sica que te gusta</h2>

			</div>
			<nav>
				<ul>
					<li><a href="index.do"><img src="images/biblioteca.png"
							width="20" height="20"></a></li>
					<li><a href="lista_reproduccion.do">Lista de Reproducci�n</a></li>
					<li><a href="lista_favoritos.do">Favoritos</a></li>
					<a href="#"><img src="images/logo-login.png"
						alt="Iniciar Sesi�n" width="30" height="30"></a>
					<a href="#" class="login-user"><%=user.getNombreUsuario()%></a>

				</ul>
			</nav>
		</header>
	</div>

   <!-- content -->
    <div class="wrapper row2">


      <div id="container" class="clear">



        <!-- Content -->
        <div id="homepage">



          <section class="clear">
            <div class="spacer"></div>
            <div class="clearfix"></div>



            <!-- ########################################################################################## -->
            <section class="artistas clear">

              <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->

              <!--             /* <div id="video-reproductor"></div>*/-->
<!--              <div class="video-options">The picture above is 350px wide. The total width of this element is also 350px.</div>-->

              <div class="spacer"></div>
              <div id="video-reproductor"></div>
              <!-- Start Video Post -->
              <div class="video-post-wrapper">

                <div class="video-posts-video">
                  <!-- Video removido de aqui-->
                </div>
                <div class="video-posts-data">
               
                  <div class="video-post-title">
                    <span class="video-icons"><i class="fa fa-info-circle"></i></span>
                    <div class="video-post-info">
                      <h4><a href="#"><%=artista.getNombreArtista()%> - <%=artista.getNumVideos()%></a></h4>
                      <div class="video-post-date">
                        <span><i class="fa fa-calendar"></i></span>
                        <p>24 junio, 2017</p>
                        <span class="video-posts-author">
                          <i class="fa fa-folder-o"></i>                          
                        </span>
                        <a href="#">POP LATINO</a><!-- cambiado por fuera para que no modifique tama�o fa fa -->
                      </div>
                    </div>
                  </div>
                  <div class="video-post-counter">
                    <div class="video-post-viewers">
                      <h3>2.151.653.266 visualizaciones</h3>
                    </div>
                    <div class="video-like">
                      <span><i class="fa fa-thumbs-o-up"></i></span>
                      <p>421825</p>
                    </div>
                    <div class="video-dislike">
                      <span><i class="fa fa-thumbs-o-down"></i></span>
                      <p>9694</p>
                  
                    </div>
                  </div>
                </div>
                <!-- Start Tags And Share Options -->
                <div class="tags-and-share video-share">
                  <div class="btn_Reproductor">
                    <button type="submit"  class="btn_Suscribe btn_Red">
                      <img src="images/video_icon_white.png" width="15" height="15" alt="submit" /><span> Suscribirse</span> 
                    </button>
                  </div>
                  <div class="share-options">
                    <h4>Compartir en..</h4>
                    <ul class="social-share">
                      <li class="twitter-bg"><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li class="facebook-bg"><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li class="google-bg"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                  </div>
                </div>
                <!-- End Tags And Share Options -->
                <div class="video-post-text">
                  <p class="small">Publicado el 21 abr. 2017</p>
                  <p class="small"> Maluma - "Felices los 4" (Official Music Video)</p>
                  <p class="small">"Felices los 4" is available on these digital platforms!
                    iTunes: http://smarturl.it/FelicesLos4</p>

                </div>

              </div>

            </section>



            <article class="videos">

              <!--<h4>Resize the browser window to see the effect.</h4>-->
              <br>

              <div class="clearfix"></div>



              <div class="clearfix"></div>







            </article>



          </section>
          <!-- ########################################################################################## -->

          <!-- ########################################################################################## -->
        </div>
        <!-- / content body -->
      </div>
    </div>
	<!-- Footer -->
	<div class="wrapper row3">
		<footer id="footer" class="clear">
			<p class="fl_left">
				Copyright &copy; 2017 - Derechos Reservados - <a href="#">VevoPlayList</a>
			</p>
			<p class="fl_right">
				Dise�ado por <a href="" title="">Videos Stolen</a>
			</p>
		</footer>
	</div>
	<script async type='text/javascript'
		src='https://use.fontawesome.com/493ef35f7a.js'></script>
</body>

</html>
