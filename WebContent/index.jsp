
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" session="true" import="entidades.*"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page	import="jersey.repackaged.com.google.common.util.concurrent.UncheckedExecutionException"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page	import="modelo.dao.ArtistaRepo,java.util.List,java.util.ArrayList,java.util.Set,java.util.Random,java.util.HashSet"%>
<%@page import="modelo.negocio.GestionArtista"%>
<%@page import="modelo.negocio.GestionVideo"%>
<!DOCTYPE html>
<html>
<head>
<title>VevoPlayList Stolen Videos</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" type="image/png" href="images/video_icon.png">
<link rel="stylesheet" href="css/style.css" type="text/css">

</head>
<body>
	<%
		Usuario user = (Usuario) session.getAttribute("usuario");
	%>
	<div class="wrapper row1">
		<header id="header" class="clear">
			<div id="hgroup">
				<h1>
					<a href="#">VevoPlayList</a>
				</h1>
				<h2>Disfruta de los v�deos y la m�sica que te gusta</h2>

			</div>
			<nav>
				<ul>
					<li><a href="index.do"><img src="images/biblioteca.png" alt="inicio" 
							width="20" height="20"></a></li>
					<li><a href="lista_reproduccion.do">Lista de Reproducci�n</a></li>
					<li><a href="lista_favoritos.do">Favoritos</a></li>
					<li><a href="#"><img src="images/logo-login.png"
							alt="Iniciar Sesi�n" width="30" height="30"></a></li>
					<li>
				<a href="#" class="login-user"><%=user.getNombreUsuario()%></a>
					</li>

					</ul>
			</nav>
		</header>
	</div>

	<!-- content -->
	<div class="wrapper row2">
		<div id="container" class="clear">
			<!-- Content -->
			<div id="homepage">
				<!--<h1 class="h1">B�squeda</h1>-->
				<div class="flexsearch">
					<div class="flexsearch--wrapper">
						<form class="flexsearch--form" action="#" method="post">
							<div class="flexsearch--input-wrapper">
								<input class="flexsearch--input" type="search"
									placeholder="t�tulo, artista, g�nero">
							</div>
							<input class="flexsearch--submit" type="submit" value="&#10140;" />
						</form>
					</div>
				</div>
				<!-- ########################################################################################## -->
				<section class="clear">
					<article class="videos">
						<!--<h4>Resize the browser window to see the effect.</h4>-->
						<br>
						<%
							@SuppressWarnings("unchecked")
							List<Video> listaVideos = (List<Video>) session.getAttribute("videos");
						   int[] ndigitos = new int[12];
						    int n;
						    
						    Random random = new Random();

						 // Conjunto de videos ya usados
						    Set<Integer> alreadyUsedNumbers = new HashSet<>();

						    // Vamos a generar 12 n�meros aleatorios sin repetici�n
						    while (alreadyUsedNumbers.size()<12) {

						       // N�mero aleatorio entre 0 y 509, excluido el 510.  
						       int randomNumber = random.nextInt(627);

						       // Si no lo hemos usado ya, lo usamos y lo metemos en el conjunto de usados.
						       if (!alreadyUsedNumbers.contains(randomNumber)){
						          System.out.println("video no repetido "+randomNumber);
						          alreadyUsedNumbers.add(randomNumber);
						       }
						    
// 							for (int n = 0; n < 12; n++) {
						%>
						<!-- Inicio 0 						 -->
						<div class="responsive">
							<div class="gallery">
								<a 									href="reproduccion_videos.do?urlVideo=<%=listaVideos.get(randomNumber).getUrlVideo()%>">
									<img src="<%=listaVideos.get(randomNumber).getUrlImagen()%>"
									alt="<%=listaVideos.get(randomNumber).getUrlImagen()%>">
								</a>
								<div class="desc">
									<%
										for (int i = 0; i < (listaVideos.get(randomNumber).getArtistas().size()); i++) {
									%>
									<a
										href="detalleArtista.do?idArtista=<%=listaVideos.get(randomNumber).getArtistas().get(i).getIdArtista()%>"><%=listaVideos.get(randomNumber).getArtistas().get(i).getNombreArtista()%>
									</a>
									<%
										}
									%>
									- <a
										href="reproduccion_videos.do?urlVideo=<%=listaVideos.get(randomNumber).getUrlVideo()%>"><%=listaVideos.get(randomNumber).getNombreVideo()%></a>
								</div>
							</div>
						</div>
						<!-- Fin 1 -->
						<%
							}
						%>
						<div class="clearfix"></div>
						<div class="desctext">
							<br>
							<p>Disfruta de los v�deos y la m�sica que te gusta, sube
								material original y comparte el contenido con tus amigos, tu
								familia y el resto del mundo en PlayListVevo.</p>
							<p>Un repaso por todas las noticias de m�sica de tus
								cantantes favoritos. Los �ltimos videoclips que no te puedes
								perder y las novedades del mundo de la m�sica, como giras,
								conciertos y festivales. Todos los estilos de m�sica de la
								actualidad, las mejores playlist y las �ltimas novedades de los
								cantantes nacionales e internacionales. Te contamos an�cdotas y
								curiosidades de las grabaciones de los �lbumes que m�s te
								gustan, hacemos un repaso por sus portadas y analizamos las
								giras de grandes artistas, adem�s de contarte los pr�ximos
								lanzamientos y sus visitas a nuestro pa�s. Hacemos un
								seguimiento de todas las galas de m�sica, sus actuaciones, y
								hacemos un repaso de los nominados y los premiados adem�s de
								analizar la alfombra roja.</p>
						</div>
						<!-- ########################################################################################## -->

					</article>
					<!--	article class="one_third lastbox"></article> -->
				</section>
				<!-- ########################################################################################## -->
				<section class="artistas clear">
					<%
						@SuppressWarnings("unchecked")
						List<Artista> listaArtista = (List<Artista>) session.getAttribute("artistas");
					  int[] ndigitosA = new int[12];
					    int nA;
					    
					    Random randomA = new Random();

					 // Conjunto de artistas ya usados
					    Set<Integer> alreadyUsedNumbersA = new HashSet<>();

					    // Vamos a generar 12 n�meros aleatorios sin repetici�n
					    while (alreadyUsedNumbersA.size()<12) {

					       // N�mero aleatorio entre 0 y323, excluido el 324.  
					       int randomNumberA = randomA.nextInt(324);

					       // Si no lo hemos usado ya, lo usamos y lo metemos en el conjunto de usados.
					       if (!alreadyUsedNumbersA.contains(randomNumberA)){
					          System.out.println("artista no repetido "+randomNumberA);
					          alreadyUsedNumbersA.add(randomNumberA);
					       }
					//for (Artista i : listaArtista) {
					%>
					<div class="responsive">
						<div class="responsive-item-content">
							<a href="detalleArtista.do?idArtista=<%=listaArtista.get(randomNumberA).getIdArtista()%>">
								<img class="circular" src="<%=listaArtista.get(randomNumberA).getUrlImagen()%>"
								alt="<%=listaArtista.get(randomNumberA).getNombreArtista()%>">
							</a> <i class="fa fa-star fa-2x aria"></i>
							<div class="descA"><%=listaArtista.get(randomNumberA).getNombreArtista()%></div>

						</div>
					</div>
					<%
						}
					%>
					<div class="spacer"></div>
				</section>
				<!-- ########################################################################################## -->
			</div>
			<!-- / content body -->
		</div>
	</div>
	<!-- Footer -->
	<div class="wrapper row3">
		<footer id="footer" class="clear">
			<p class="fl_left">
				Copyright &copy; 2017 - Derechos Reservados - <a href="#">VevoPlayList</a>
			</p>
			<p class="fl_right">
				Dise�ado por <a href="#" title="Videos Stolen">Videos Stolen</a>
			</p>
		</footer>
	</div>
	<script async type='text/javascript'
		src='https://use.fontawesome.com/493ef35f7a.js'></script>

</body>
</html>
